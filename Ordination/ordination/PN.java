package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> datoDosis = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel,
			double antalEnheder) {
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean result = false;

		if (givesDen.isBefore(getSlutDen().plusDays(1))
				&& givesDen.isAfter(getStartDen().minusDays(1))) {

			datoDosis.add(givesDen);
			result = true;
		} else {
			throw new IllegalArgumentException(
					"Ordinationen er ikke givet indenfor gyldighedsperioden");
		}
		return result;
	}

	@Override
	public double doegnDosis() {
		LocalDate first;
		LocalDate last;
		if (datoDosis.size() == 0) {
			return 0;
		} else {
			first = datoDosis.get(datoDosis.size() - 1);
			last = datoDosis.get(datoDosis.size() - 1);
			double result = 0;
			for (LocalDate d : datoDosis) {
				if (d.isBefore(first)) {
					first = d;
				} else if (d.isAfter(last)) {
					last = d;
				}
			}
			result = samletDosis() / (ChronoUnit.DAYS.between(first, last) + 1);
			return result;
		}
	}

	@Override
	public double samletDosis() {
		return getAntalGangeGivet() * getAntalEnheder();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return datoDosis.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		String type = "PN";
		return type;
	}

}
