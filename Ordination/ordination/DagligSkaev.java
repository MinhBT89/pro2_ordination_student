package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	private ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen,
			Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
	}

	/**
	 *
	 * @param tid
	 * @param antal
	 */

	public void opretDosis(LocalTime tid, double antal) {
		Dosis d = new Dosis(tid, antal);
		doser.add(d);
	}

	public ArrayList<Dosis> getDoser() {
		ArrayList<Dosis> tempDoser = new ArrayList<>();
		tempDoser = doser;
		return tempDoser;
	}

	@Override
	public double samletDosis() {
		double i = 0;
		for (Dosis d : doser) {
			i += d.getAntal();
		}
		return i * antalDage();
	}

	@Override
	public double doegnDosis() {
		double i = 0;
		for (Dosis d : doser) {
			i += d.getAntal();
		}
		return i;
	}

	@Override
	public String getType() {
		String type = "Daglig Skæv";
		return type;
	}

}
