package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.BeforeClass;
import org.junit.Test;

public class DagligSkaevTest {

	private static DagligSkaev d = new DagligSkaev(LocalDate.now(), LocalDate
			.now().plusDays(3), null);
	private static DagligSkaev d1 = new DagligSkaev(LocalDate.now(), LocalDate
			.now().plusDays(3), null);

	@BeforeClass
	public static void setupBefore() {
		d.opretDosis(LocalTime.parse("06:00:00"), 2);
		d.opretDosis(LocalTime.parse("12:00:00"), 2);
		d.opretDosis(LocalTime.parse("18:00:00"), 2);
		d.opretDosis(LocalTime.parse("23:59:59"), 2);
		d1.opretDosis(LocalTime.parse("06:00:00"), 2);
		d1.opretDosis(LocalTime.parse("12:00:00"), 2);
		d1.opretDosis(LocalTime.parse("18:00:00"), 2);
		d1.opretDosis(LocalTime.parse("23:59:59"), 2);
		d1.opretDosis(LocalTime.parse("06:00:00"), 2);
		d1.opretDosis(LocalTime.parse("12:00:00"), 2);
		d1.opretDosis(LocalTime.parse("18:00:00"), 2);
		d1.opretDosis(LocalTime.parse("23:59:59"), 2);
	}

	@Test
	public void testSamletDosis() {
		d.setStartDen(LocalDate.now());
		d.setSlutDen(LocalDate.now());
		assertEquals(8, d.samletDosis(), 0.01);
	}

	@Test
	public void testSamletDosis1() {
		d.setStartDen(LocalDate.now());
		d.setSlutDen(LocalDate.now().plusDays(1));
		assertEquals(16, d.samletDosis(), 0.01);
	}

	@Test
	public void testSamletDosis2() {
		d1.setStartDen(LocalDate.now());
		d1.setSlutDen(LocalDate.now());
		assertEquals(16, d1.samletDosis(), 0.01);
	}

	@Test
	public void testSamletDosis3() {
		d1.setStartDen(LocalDate.now());
		d1.setSlutDen(LocalDate.now().plusDays(1));
		assertEquals(32, d1.samletDosis(), 0.01);
	}

}
