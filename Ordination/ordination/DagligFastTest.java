package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.BeforeClass;
import org.junit.Test;

public class DagligFastTest {

	private static DagligFast d = new DagligFast(null, null, null);
	private static DagligFast d1 = new DagligFast(null, null, null);

	@BeforeClass
	public static void setupBefore() {
		d.opretDosis(LocalTime.parse("06:00:00"), 2, 0);
		d.opretDosis(LocalTime.parse("12:00:00"), 2, 1);
		d.opretDosis(LocalTime.parse("18:00:00"), 2, 2);
		d.opretDosis(LocalTime.parse("23:59:59"), 2, 3);
		d1.opretDosis(LocalTime.parse("06:00:00"), 4, 0);
		d1.opretDosis(LocalTime.parse("12:00:00"), 4, 1);
		d1.opretDosis(LocalTime.parse("18:00:00"), 4, 2);
		d1.opretDosis(LocalTime.parse("23:59:59"), 4, 3);
	}

	@Test
	public void testSamletDosis() {
		d.setStartDen(LocalDate.now());
		d.setSlutDen(LocalDate.now());
		assertEquals(8, d.samletDosis(), 0.01);
	}

	@Test
	public void testSamletDosis1() {
		d.setStartDen(LocalDate.now());
		d.setSlutDen(LocalDate.now().plusDays(1));
		assertEquals(16, d.samletDosis(), 0.01);
	}

	@Test
	public void testSamletDosis2() {
		d1.setStartDen(LocalDate.now());
		d1.setSlutDen(LocalDate.now());
		assertEquals(16, d1.samletDosis(), 0.01);
	}

	@Test
	public void testSamletDosis3() {
		d1.setStartDen(LocalDate.now());
		d1.setSlutDen(LocalDate.now().plusDays(1));
		assertEquals(32, d1.samletDosis(), 0.01);
	}

}
