package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen,
			Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
	}

	public void opretDosis(LocalTime tid, double antal, int position) {
		Dosis d = new Dosis(tid, antal);
		doser[position] = d;
	}

	public Dosis[] getDoser() {
		Dosis[] tempDoser = new Dosis[4];
		tempDoser = doser;
		return tempDoser;
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double dosis = 0;
		for (Dosis d : doser) {
			dosis += d.getAntal();
		}
		return dosis;
	}

	@Override
	public String getType() {
		String type = "Daglig Fast";
		return type;
	}
}