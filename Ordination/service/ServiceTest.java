package service;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import storage.Storage;

public class ServiceTest {
	private static Service s = new Service();

	@BeforeClass
	public static void setupBefore() {
		s.createSomeObjects();
	}

	// ----------------------------------------------------------------------------
	// --------------------------Vægt 1 Til 150------------------------------------
	// ----------------------------------------------------------------------------
	
	@Test
	public void testLaegemiddel0Vægt1Til150() {
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(1, 150, Storage
				.getAllLaegemidler().get(0)));
	}

	@Test
	public void testLaegemiddel1Vægt1Til150() {
		assertEquals(3, s.antalOrdinationerPrVægtPrLægemiddel(1, 150, Storage
				.getAllLaegemidler().get(1)));
	}

	@Test
	public void testLaegemiddel2Vægt1Til150() {

		assertEquals(2, s.antalOrdinationerPrVægtPrLægemiddel(1, 150, Storage
				.getAllLaegemidler().get(2)));
	}

	@Test
	public void testLaegemiddel3Vægt1Til150() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(1, 150, Storage
				.getAllLaegemidler().get(3)));
	}

	// ----------------------------------------------------------------------------
	// --------------------------Vægt 1 Til 59-------------------------------------
	// ----------------------------------------------------------------------------
	@Test
	public void testLaegemiddel0Vægt1Til59() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(1, 59, Storage
				.getAllLaegemidler().get(0)));
	}

	@Test
	public void testLaegemiddel1Vægt1Til59() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(1, 59, Storage
				.getAllLaegemidler().get(1)));
	}

	@Test
	public void testLaegemiddel2Vægt1Til59() {

		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(1, 59, Storage
				.getAllLaegemidler().get(2)));
	}

	@Test
	public void testLaegemiddel3Vægt1Til59() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(1, 59, Storage
				.getAllLaegemidler().get(3)));
	}

	// ----------------------------------------------------------------------------
	// ------------------------------Vægt 90 Til 300-------------------------------
	// ----------------------------------------------------------------------------
	@Test
	public void testLaegemiddel0Vægt90Til300() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(90, 300, Storage
				.getAllLaegemidler().get(0)));
	}

	@Test
	public void testLaegemiddel1Vægt90Til300() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(90, 300, Storage
				.getAllLaegemidler().get(1)));
	}

	@Test
	public void testLaegemiddel2Vægt90Til300() {

		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(90, 300, Storage
				.getAllLaegemidler().get(2)));
	}

	@Test
	public void testLaegemiddel3Vægt90Til300() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(90, 300, Storage
				.getAllLaegemidler().get(3)));
	}

	// ----------------------------------------------------------------------------
	// ---------------------------Vægt 1 Til 63------------------------------------
	// ----------------------------------------------------------------------------
	@Test
	public void testLaegemiddel0Vægt1Til63() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(1, 63, Storage
				.getAllLaegemidler().get(0)));
	}

	@Test
	public void testLaegemiddel1Vægt1Til63() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(1, 63, Storage
				.getAllLaegemidler().get(1)));
	}

	@Test
	public void testLaegemiddel2Vægt1Til63() {

		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(1, 63, Storage
				.getAllLaegemidler().get(2)));
	}

	@Test
	public void testLaegemiddel3Vægt1Til63() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(1, 63, Storage
				.getAllLaegemidler().get(3)));
	}

	// ----------------------------------------------------------------------------
	// ----------------------------Vægt 1 Til 83-----------------------------------
	// ----------------------------------------------------------------------------
	@Test
	public void testLaegemiddel0Vægt1Til83() {
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(1, 83, Storage
				.getAllLaegemidler().get(0)));
	}

	@Test
	public void testLaegemiddel1Vægt1Til83() {
		assertEquals(2, s.antalOrdinationerPrVægtPrLægemiddel(1, 83, Storage
				.getAllLaegemidler().get(1)));
	}

	@Test
	public void testLaegemiddel2Vægt1Til83() {

		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(1, 83, Storage
				.getAllLaegemidler().get(2)));
	}

	@Test
	public void testLaegemiddel3Vægt1Til83() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(1, 83, Storage
				.getAllLaegemidler().get(3)));
	}

	// ----------------------------------------------------------------------------
	// -----------------------------Vægt 60 Til 90---------------------------------
	// ----------------------------------------------------------------------------
	@Test
	public void testLaegemiddel0Vægt60Til90() {
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(60, 90, Storage
				.getAllLaegemidler().get(0)));
	}

	@Test
	public void testLaegemiddel1Vægt60Til90() {
		assertEquals(3, s.antalOrdinationerPrVægtPrLægemiddel(60, 90, Storage
				.getAllLaegemidler().get(1)));
	}

	@Test
	public void testLaegemiddel2Vægt60Til90() {
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(60, 90, Storage
				.getAllLaegemidler().get(2)));
	}

	@Test
	public void testLaegemiddel3Vægt60Til90() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(60, 90, Storage
				.getAllLaegemidler().get(3)));
	}

	// ----------------------------------------------------------------------------
	// -----------------------------Vægt 64 Til 90---------------------------------
	// ----------------------------------------------------------------------------
	@Test
	public void testLaegemiddel0Vægt64Til90() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(64, 90, Storage
				.getAllLaegemidler().get(0)));
	}

	@Test
	public void testLaegemiddel1Vægt64Til90() {
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(64, 90, Storage
				.getAllLaegemidler().get(1)));
	}

	@Test
	public void testLaegemiddel2Vægt64Til90() {
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(64, 90, Storage
				.getAllLaegemidler().get(2)));
	}

	@Test
	public void testLaegemiddel3Vægt64Til90() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(64, 90, Storage
				.getAllLaegemidler().get(3)));
	}

}
